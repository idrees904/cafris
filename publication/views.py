from django.shortcuts import render
from rest_framework import generics
from .serializers import *
from .models import *
from .permissions import IsOwnerOrReadOnly
# только авторизованные пользователи
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
import json

import services.parse_eis as pe


class PlanCreateView(generics.CreateAPIView):
    """ Запись информации о запросе на публикацию """
    # Необходимо дописать permissions на проверку доступа пользователя к запросам
    serializer_class = PlanDetailSerializar

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            # print('NOT ERRORS')
            # Записать информацию о запросе на публикацию
            self.create(request, *args, **kwargs)

            position_dumps = json.loads(json.dumps(serializer.data['position']))

            ikz = [pd['ident_code'] for pd in position_dumps]

            reg_num = serializer.data['reg_num']
            result_data = {}
            pe.get_information_for_PG(reg_num, result_data, ikz)

            # Зачистить code_result
            result_data.pop('code_result', None)
            # Объеденить запрос на публикацию и результат полученный с ЕИС по этому запросу
            request_data = serializer.data
            request_data['result_search'] = result_data

            return Response(request_data)
        else:
            # print('ERRORS')
            serializer.is_valid(raise_exception=True)
            return Response(serializer.data)


class PlanListView(generics.ListAPIView):
    """ Генерация списка всех записей """
    serializer_class = PlanListViewSerializar
    queryset = Plan.objects.all()
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly, ]


class PlanDetailView(generics.RetrieveUpdateDestroyAPIView):
    """ Просмотр, Изменение, Удаление по одной зиписи. """
    serializer_class = PlanDetailSerializar
    queryset = Plan.objects.all()
    # Изменять или удалять запись может только аутентифицированный пользователь и только ее автор
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly, ]
