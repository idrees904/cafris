# Generated by Django 2.1.15 on 2020-10-02 04:07

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0002_publicationrequest_ident_code_position'),
    ]

    operations = [
        migrations.CreateModel(
            name='IdentCodePosition',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('ident_code', models.CharField(max_length=36, verbose_name='ИКЗ')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='publicationrequest',
            name='ident_code_position',
        ),
        migrations.AddField(
            model_name='identcodeposition',
            name='rid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ident_code', to='publication.PublicationRequest'),
        ),
    ]
