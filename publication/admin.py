from django.contrib import admin
from django.utils.html import format_html
from .models import Plan


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = ('link', 'date', 'reg_num', 'number', 'version', 'date_pub', 'date_doc', 'inn_org', 'name_org', 'url_doc', 'user_id')

    def ris_url_doc(self, obj):
        return format_html("<a href='{url}'>{url_name}</a>", url=obj.url_doc, url_name=obj.link)

    ris_url_doc.allow_tags = True
    ris_url_doc.short_description = "RIS URL Doc"

    list_filter = ('link', 'date', 'number')
