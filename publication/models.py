from django.db import models
import uuid
from django.utils import timezone
from django.contrib.auth import get_user_model
User = get_user_model()


class AbstractUUID(models.Model):
    """
    Абстрактная модель для использования UUID в качестве PK.
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    
    class Meta:
        abstract = True


class Plan(AbstractUUID):
    """
    Модель хранения запросов пользователей на перевод документов
    """
    link = models.IntegerField(verbose_name='Link в РИС', blank=False, null=False, db_index=True)
    date = models.DateTimeField(verbose_name='Дата добавления записи', auto_now_add = False, editable=False, default=timezone.now)
    reg_num = models.CharField(verbose_name='Рег. номер', max_length=100, blank=False, null=False)
    number = models.CharField(verbose_name='Номер', max_length=100, blank=False, null=False)
    version = models.IntegerField(verbose_name='Версия', blank=False, null=False)
    date_pub = models.DateTimeField(verbose_name='Дата публикации', blank=True, null=True)
    date_doc = models.DateTimeField(verbose_name='Дата документа', blank=False, null=False)
    inn_org = models.CharField(verbose_name='ИНН Организации', max_length=13, blank=False, null=False)
    name_org = models.CharField(verbose_name='Наименование организации', max_length=200, blank=False, null=False)
    url_doc = models.CharField(verbose_name='Web ссылка в РИС', max_length=500, blank=False, null=False)
    user = models.ForeignKey(User, related_name='plan_user', verbose_name='Запросивший пользователь', on_delete=models.CASCADE)

class Position(AbstractUUID):
    plan = models.ForeignKey(Plan, related_name='position', on_delete=models.CASCADE, db_index=True)
    date = models.DateTimeField(verbose_name='Дата добавления записи', auto_now_add = False, editable=False, default=timezone.now)
    link = models.IntegerField(verbose_name='Link в РИС', blank=False, null=False, db_index=True)
    number = models.CharField(verbose_name='Номер', max_length=100, blank=False, null=False)
    version = models.IntegerField(verbose_name='Версия', blank=False, null=False)
    data_doc = models.DateTimeField(verbose_name='Дата документа', blank=False, null=False)
    ident_code = models.CharField(verbose_name='ИКЗ', max_length=36, blank=False, null=False)
    summa = models.DecimalField(verbose_name='Сумма', max_digits=20, decimal_places=2, blank=False, null=False)
    reg_num = models.CharField(verbose_name='Рег. номер', max_length=100, blank=True, null=True)
    reg_num_new = models.CharField(verbose_name='Новый рег. номер', max_length=100, blank=True, null=True)

# class Confirm(AbstractUUID):
#     req_id = models.ForeignKey(Plan, related_name='conf_req_id', on_delete=models.CASCADE, db_index=True)
