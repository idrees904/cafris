import requests

class GET_HTML():
    """Получить html код и код статуса"""
    def __init__(self, url:str, params:str, data:dict):
        """init args"""
        self.url = url
        self.payload = params
        self.data = data
        self.header = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0',}

    def get_html_code(self):
        """Получить HTML код страницы"""
        try:
            req = requests.get(self.url, headers=self.header, params=self.payload, timeout=3)
            self.data['url_request'] = req.url
            self.data['code_result'] = str(req.text)
        except requests.exceptions.HTTPError as errh:
            self.data['error_get_html'] = "Http Error: " + str(errh)
        except requests.exceptions.ConnectionError as errc:
            self.data['error_get_html'] = "Error Connecting: " + str(errc)
        except requests.exceptions.Timeout as errt:
            self.data['error_get_html'] = "Timeout Error: " + str(errt)
        except requests.exceptions.RequestException as err:
            self.data['error_get_html'] = "There was an ambiguous exception that occurred while handling request. " + str(err)
        return self.data