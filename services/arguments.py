
def arguments(reg_num:str):
    """Аргументы для GEТ запроса"""
    data = dict(
        base_url = 'https://zakupki.gov.ru',
        search_url = 'https://zakupki.gov.ru/epz/orderplan/search/results.html',
        search_payload = {
            'searchString':reg_num,
            'structured':'true',
            'fz44':'on',
            'orderPlanSearchPlace':'PLANS_AND_DOCUMENTS',
            'sortBy':'BY_MODIFY_DATE',
        },
        )
    return data