from rest_framework import serializers
from .models import *
import json

class PositionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ('date','link','number','version','data_doc','ident_code','summa',)

class PlanDetailSerializar(serializers.ModelSerializer):
    """
    docstring
    """
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    position = PositionListSerializer(many=True)

    class Meta:
        model = Plan
        fields = '__all__'

    def create(self, validated_data):
        position_data = validated_data.pop('position')
        plan = Plan.objects.create(**validated_data)
        for pos in position_data:
            Position.objects.create(plan=plan, **pos)
        return plan


class PlanListViewSerializar(serializers.ModelSerializer):
    """
    docstring
    """
    class Meta:
        model = Plan
        fields = '__all__'

