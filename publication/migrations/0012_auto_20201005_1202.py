# Generated by Django 2.1.15 on 2020-10-05 01:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('publication', '0011_auto_20201005_1057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='position',
            name='req_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='positions_ris', to='publication.Plan'),
        ),
    ]
