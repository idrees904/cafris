from django.urls import path, include
from .views import *


app_name = 'publication'
urlpatterns = [
    path('create/', PlanCreateView.as_view()),
    path('detail/<uuid:pk>', PlanDetailView.as_view()),
    path('all/', PlanListView.as_view()),
]
