from services.arguments import *
from services.get_html import GET_HTML as GH
from bs4 import BeautifulSoup as bs
import re
from datetime import timedelta, datetime

def add_error_in_dict(data:dict, message:list) -> None:
    """Добавляет ошибки в общий словарь"""
    if data.get('errors'):
        data['errors'].extend(message)
    else:
        data['errors'] = message

def get_html_page_search_in_eis(reg_num:str, data:dict) -> None:
    """Получает "План-график" в ЕИС по его регистрационному номеру и возвращает html код страницы"""
    arguments_dict = arguments(reg_num)
    GH(arguments_dict['search_url'], arguments_dict['search_payload'], data).get_html_code()

def get_url_on_PG_and_add_in_JSON(data:dict) -> None:
    """Получает ссылку на карточку плана-графика и добавляет ее в json"""
    if not data.get('error_get_html'):
        try:
            card = bs(data['code_result'], 'lxml').find('div', {'class':'registry-entry__header-mid__number'})
            data['part_url_card'] = card.a.get('href')
        except Exception as ex:
            message = ['Ошибка получения ссылки на карточку плана-графика: ' + str(ex)]
            add_error_in_dict(data, message)

def get_tabs_and_add_JSON(data:dict) -> None:
    """Получает все вкладки со страницы карточки плана-графика"""
    if not data.get('error_get_html'):
        GH(arguments(None)['base_url'] + data.get('part_url_card',''), None, data).get_html_code()
        try:
            tabs = bs(data['code_result'], 'lxml').find('div', {'class':'tabsNav d-flex align-items-end tabsNav-media-mr-auto'})
            
            for url_t in tabs.find_all('a'):
                key = re.search(r'\/([^/]+)\.html\??', url_t.get('href')).group(1)
                val = url_t.get('href')
                data.setdefault('tabs', {key:val}).update({key:val})
        except Exception as ex:
            message = ['Ошибка получения ссылок на вкладки карточки плана-графика: ' + str(ex)]
            add_error_in_dict(data, message)

def get_url_on_xml_and_add_in_JSON(data:dict) -> None:
    """Получает ссылка на XML плана-графика и записывает в JSON"""
    if not data.get('error_get_html'):
        url_doc = data.get('tabs',{}).get('documents-info','')
        GH(arguments(None)['base_url'] + url_doc, None, data).get_html_code()
        try:
            xml_div = bs(data['code_result'], 'lxml').find('div', {'class':'noticeTabBoxWrapper notice-documents padding-0-15px-15px-5px'})
            data['url_xml'] = xml_div.find('a', {'class':'printForm'}).get('href').replace('view.html', 'viewXml.html', 1)
        except Exception as ex:
            message = ['Не удалось получить ссылку на XML: ' + str(ex)]
            add_error_in_dict(data, message)

def get_xml_and_add_in_JSON(data:dict) -> None:
    """Получает XML плана-графика и записывает в JSON"""
    url_xml = data.get('url_xml')
    if url_xml:
        GH(arguments(None)['base_url'] + url_xml, None, data).get_html_code()

def get_info_about_confirm_doc_from_XML(data:dict):
    """Получает из XML информацию по подтвердившем документ лице 'confirmcontactinfo' и дате"""
    try:
        cci = bs(data['code_result'], 'lxml').find('confirmcontactinfo')
        lastname = cci.find('lastname').text
        firstname = cci.find('firstname').text
        middlename = cci.find('middlename').text
        position = cci.find('position').text

        data['confirmcontactinfo'] = {'fio':lastname+' '+firstname+' '+middlename, 'position':position}
    except Exception as ex:
        message = ['Не удалось получить данные о подписи: ' + str(ex)]
        add_error_in_dict(data, message)
    
    try:
        conf_d = bs(data['code_result'], 'lxml').find('confirmdate')
        date_h = datetime.strptime(conf_d.text, '%Y-%m-%dT%H:%M:%S')
        date_pub_event_end = str(date_h + timedelta(hours=8))
        data['confirmcontactinfo'].update({'confirmdate':date_pub_event_end})
    except Exception as ex:
        message = ['Не удалось получить данные о дате подписи: ' + str(ex)]
        add_error_in_dict(data, message)
    
    return data
    
def get_position(data:dict, ikz:list) -> None:
    """Получает реестровые номера из XML(ЕИС) для заранее заданного списка ИКЗ"""
    try:
        xml_dt = bs(data['code_result'], 'lxml')
        regular_positions = xml_dt.find('positions')
        special_positions = xml_dt.find('specialpurchasepositions')

        data.update({'positions':{}, 'specialpositions':{}})

        for i in ikz:
            if regular_positions:
                reg_pos = regular_positions.find(text=str(i))
                if reg_pos:
                    reg_n = reg_pos.parent.parent.parent.find('positionnumber').text
                    data['positions'].update({i:reg_n})
                    continue
            if special_positions:
                spe_pos = special_positions.find(text=str(i))
                if spe_pos:
                    reg_n = spe_pos.parent.parent.find('positionnumber').text
                    data['specialpositions'].update({i:reg_n})
    except Exception as ex:
        message = ['Не удалось получить данные о реестровых номерах позиций плана-графика: ' + str(ex)]
        add_error_in_dict(data, message)
    return data

def get_version_publication(data:dict) -> None:
    """Получает версию публикации документа план-график в ЕИС"""
    try:
        ver = bs(data['code_result'], 'lxml').find('versionnumber')
        data.update({'versionnumber':int(ver.text)})
    except Exception as ex:
        message = ['Не удалось получить данные о версии: ' + str(ex)]
        add_error_in_dict(data, message)
    return data

def get_url_on_date_publication(data:dict):
    """Получает ссылку на дату публикации плана-графика в ЕИС"""
    if data.get('tabs'):
        for key, values in data['tabs'].items():
            if key.find("event-") == 0:
                event = arguments(None)['base_url'] + values

                GH(event, None, data).get_html_code()
    try:
        js_parse = bs(data['code_result'], 'lxml')
        js_parse = js_parse.find('div', {'class':'cardWrapper outerWrapper'})
        url = re.search(r"url:\s'(.*)',", str(js_parse)).group(1)
        sid = re.search(r"sid:\s'(\d*)'", str(js_parse)).group(1)
        qualifier = re.search(r"qualifier:\s'(\w+\d*)'", str(js_parse)).group(1)

        data['part_url_date_publication'] = url+'?sid='+sid+'&page=1&pageSize=10&qualifier='+qualifier
    except Exception as ex:
        message = ['Не удалось получить ссылку на данные о дате публикации плана-графика: ' + str(ex)]
        add_error_in_dict(data, message)
    return data


def get_data_publication(data:dict) -> None:
    """Получает дату побликации плана-графика в ЕИС"""
    GH(arguments(None)['base_url'] + data.get('part_url_date_publication',''), None, data).get_html_code()
    try:
        original_dp = bs(data['code_result'], 'lxml')
        original_dp = original_dp.find('td', {'class':'table__cell table__cell-body'})
        date_pub = original_dp.text.strip()
        date_pub = datetime.strptime(date_pub, "%d.%m.%Y %H:%M") + timedelta(hours=8)

        data.update({'date_publication':str(date_pub)})
    except Exception as ex:
        message = ['Не удалось получить данные о дате публикации плана-графика: ' + str(ex)]
        add_error_in_dict(data, message)
    return data

def get_information_for_PG(reg_num:str, data:dict, ikz:list):
    """Собирает необходимую информацию для перевода плана-графика в опубликованные"""
    get_html_page_search_in_eis(reg_num, data)
    get_url_on_PG_and_add_in_JSON(data)
    get_tabs_and_add_JSON(data)
    get_url_on_xml_and_add_in_JSON(data)
    get_xml_and_add_in_JSON(data)
    get_info_about_confirm_doc_from_XML(data)
    get_position(data, ikz)
    get_version_publication(data)
    get_url_on_date_publication(data)    
    get_data_publication(data)
    return data

